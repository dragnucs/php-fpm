ARG PHP_VERSION=7.3

FROM php:$PHP_VERSION-fpm

LABEL maintainer="Mohamed-Touhami MAHDI <touhami@touha.me>"

ENV COMPOSER_ALLOW_SUPERUSER 1
ENV COMPOSER_HOME /var/composer
ENV COMPOSER_MEMORY_LIMIT -1

RUN set -ex; \
    apt-get update && apt-get install -y --no-install-recommends \
      libpq-dev \
      librabbitmq-dev \
      libfreetype6-dev \
      libmagickwand-dev \
      zip \
      unzip \
    ; \
    rm -rf /var/lib/apt/lists/*; \
    pecl install amqp apcu imagick redis; \
    docker-php-ext-enable amqp apcu imagick redis; \
    docker-php-ext-install -j$(nproc) pdo_pgsql mbstring iconv sockets gd; \
    { \
        echo "memory_limit=1G"; \
        echo "upload_max_filesize = 10M"; \
        echo "post_max_size = 8M"; \
        echo "expose_php = Off"; \
    } > /usr/local/etc/php/conf.d/app.ini

COPY --from=composer:2.0 /usr/bin/composer /usr/bin/composer

VOLUME /var/composer
